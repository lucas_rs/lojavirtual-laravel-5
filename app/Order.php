<?php

namespace LojaVirtual;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['user_id', 'total', 'status'];

    public function items()
    {
        return $this->hasMany('LojaVirtual\OrderItem');
    }

    public function user()
    {
        return $this->belongsTo('LojaVirtual\User');
    }
}
