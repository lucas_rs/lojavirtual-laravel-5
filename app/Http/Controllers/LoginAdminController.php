<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use LojaVirtual\Http\Requests;

class LoginAdminController extends Controller
{
    public function index()
    {
        return view('admin.login.index');
    }


    public function authUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $values = [
            'email'    => $request->get('email'),
            'password' => $request->get('password'),
            'is_admin' => 1
        ];
        if (Auth::attempt($values)) {
            return redirect()->route('categories');
        }
        return view('admin.login.index')->withErrors("Email ou senha inválidos", "fail");
    }

    public function logoutUser()
    {
        Auth::logout();
        return redirect()->route('login.admin.index');
    }

}
