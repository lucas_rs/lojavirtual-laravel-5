<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Http\Request;

use LojaVirtual\Http\Requests;
use LojaVirtual\Http\Controllers\Controller;
use LojaVirtual\User;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('store.client-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('store.client-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ClientRequest $clientRequest
     * @return Response
     * @internal param Request $request
     */
    public function store(Requests\ClientRequest $clientRequest)
    {
        User::create([
            'name' => $clientRequest->get('name'),
            'email' => $clientRequest->get('email'),
            'password' => bcrypt($clientRequest->get('password')),
        ]);
        return redirect()->route('login.client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
