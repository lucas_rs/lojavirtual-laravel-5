<?php namespace LojaVirtual\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use LojaVirtual\Category;
use LojaVirtual\Http\Requests;
use LojaVirtual\Product;
use LojaVirtual\ProductImages;
use LojaVirtual\Tag;

class ProductsController extends Controller
{

    private $productsModel;


    public function __construct(Product $productModel)
    {
        return $this->productsModel = $productModel;
    }


    /**
     * @return \List all products from @productsModel
     */
    public function index()
    {
        $products = $this->productsModel->orderBy('id', 'DESC')->paginate(10);
        return view('admin.products.index', compact('products'));
    }


    /**
     * @param Category $category
     * @return Show form to create a new product
     */
    public function create(Category $category)
    {
        $categories = $category->lists('name', 'id');
        return view('admin.products.create', compact('categories'));
    }


    /**
     * @param Requests\ProductRequest $request
     * @return \Get values from input and save in database Products
     */
    public function store(Requests\ProductRequest $request)
    {
        //Get Tags from input tags
        $tagsValues = $this->getTags($request->get('tags'));

        //Record a new product
        $product = $this->productsModel->fill($request->all());
        $product->save();

        // Sync inputs tags related with a product
        $product->tags()->sync($tagsValues);

        return redirect()->route('products');
    }


    /**
     * @param $tags
     * @return Return all id tags in array
     */
    private function getTags($tags)
    {
        $tags = explode(",", $tags);
        $tags = array_map('trim', $tags);

        $tagsAll = [];
        foreach ($tags as $tag) {
            $t = Tag::firstOrCreate(['name' => $tag]);
            array_push($tagsAll, $t->id);
        }
        return $tagsAll;
    }


    /**
     * @param $id
     * @param Category $category
     * @return Get $id data and return a form to edit values
     */
    public function edit($id, Category $category)
    {
        $product = $this->productsModel->find($id);
        $categories = $category->lists('name', 'id');
        $tags = $product->TagList;
        return view('admin.products.edit', compact('product', 'categories', 'tags'));
    }


    /**
     * @param Requests\ProductRequest $request
     * @param $id
     * @return Get values from product, update and redirect to index products list
     */
    public function update(Requests\ProductRequest $request, $id)
    {
        //Get Tags from input tags
        $tagsValues = $this->getTags($request->get('tags'));

        //update product and tags
        $product = $this->productsModel->find($id)->update($request->all());
        $p = $this->productsModel->find($id);
        $p->tags()->sync($tagsValues);

        return redirect()->route('products');
    }


    /**
     * @param ProductImages $productImages
     * @param $id
     * @return Delete data and redirect for the list products page
     */
    public function destroy(ProductImages $productImages, $id)
    {
        $images = $productImages->where('product_id', '=', $id)->get();
        foreach ($images as $img) {
            Storage::disk('local_public')->delete($img->file);
        }
        $this->productsModel->find($id)->delete();
        return redirect()->route('products');
    }


    /**
     * @param $id
     * @return \Show all images related with all products
     */
    public function images($id)
    {
        $product = $this->productsModel->find($id);
        return view('admin.products.images.index', compact('product'));
    }


    /**
     * @param $id
     * @return \Show form to make upload images
     */
    public function createImage($id)
    {
        $product = $this->productsModel->find($id);
        return view('admin.products.images.create', compact('product'));
    }


    /**
     * @param Requests\ProductImagesRequest $request
     * @param $id
     * @param ProductImages $productImages
     * @return \Get file from input, make upload to path and save a new name in database
     */
    public function storeImage(Requests\ProductImagesRequest $request, $id, ProductImages $productImages)
    {
        $file = $request->file('file');
        $name = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
        $productImages::create([
            'product_id' => $id,
            'file' => $name
        ]);
        Storage::disk('local_public')->put($name, File::get($file));
        return redirect()->route('products.images', ['id' => $id]);
    }


    /**
     * @param ProductImages $productImages
     * @param $id
     * @return \Destroy image from path and delete from database
     */
    public function destroyImage(ProductImages $productImages, $id)
    {
        $image = $productImages->find($id);
        if (file_exists(public_path() . '/uploads/' . $image->file)) {
            Storage::disk('local_public')->delete($image->file);
        }

        $product = $image->product;
        $image->delete();
        return redirect()->route('products.images', ['id' => $product->id]);
    }

}
