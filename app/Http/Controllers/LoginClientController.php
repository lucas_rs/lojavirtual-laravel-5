<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use LojaVirtual\Http\Requests;
use Illuminate\Support\Facades\Auth;
use LojaVirtual\Http\Controllers\Controller;

class LoginClientController extends Controller
{
    public function index()
    {
        return view('store.login');
    }

    public function authUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $values = [
            'email'    => $request->get('email'),
            'password' => $request->get('password')
        ];
        if (Auth::attempt($values)) {
            return redirect()->route('client.index');
        }
        return view('store.login')->withErrors("Email ou senha inválidos", "fail");
    }

    public function logoutUser()
    {
        Auth::logout();
        return redirect()->to('/');
    }
}
