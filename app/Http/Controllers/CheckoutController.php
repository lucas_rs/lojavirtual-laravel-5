<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use LojaVirtual\Http\Requests;
use LojaVirtual\Order;
use LojaVirtual\OrderItem;

class CheckoutController extends Controller
{


    public function place(Order $orderModel, OrderItem $orderItem)
    {
        if ( ! Session::has('cart')) {
            return redirect()->route('cart');
        }


        $cart = Session::get('cart');
        if ($cart->getTotal() > 0) {
            $order = $orderModel->create([
                'user_id' => Auth::user()->id,
                'total'   => $cart->getTotal()
            ]);

            foreach ($cart->all() as $k => $item) {
                $order->items()->create([
                    'product_id' => $k,
                    'price'      => $item['price'],
                    'qtd'        => $item['qtd']
                ]);
            }

            $cart->clear();
            dd($order->items);
        }
        return redirect()->route('cart');
    }


}
