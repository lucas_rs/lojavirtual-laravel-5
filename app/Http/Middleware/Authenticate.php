<?php namespace LojaVirtual\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $status = Auth::check();

        if ($status == false) {
            //return redirect()->back();
            return redirect()->route('login.admin.index');
        }

        if (Auth::user()->is_admin == "0") {
            return redirect()->route('client.index');
        }
        return $next($request);

    }

}
