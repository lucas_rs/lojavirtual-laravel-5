<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use LojaVirtual\Product;
use Faker\Factory as Faker;

class ProductsTableSeed extends Seeder
{

    public function run()
    {
        DB::table('products')->truncate();

        $faker = Faker::create('pt_BR');

        foreach (range(1, 40) as $i) {
            Product::create([
                'name' => $faker->word(),
                'description' => $faker->sentence(),
                'price' => $faker->randomNumber(2),
                'featured' => 0,
                'recommended' => 0,
                'category_id' => $faker->numberBetween(1, 15)
            ]);
        }

    }

}