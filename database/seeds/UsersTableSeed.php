<?php

use Illuminate\Database\Seeder;

class UsersTableSeed extends Seeder
{

    public function run()
    {
        DB::table('users')->truncate();

        factory('LojaVirtual\User')->create([
            'name'     => "Luciano Guerra",
            'email'    => "dsibr@hotmail.com",
            'password' => bcrypt('1234'),
            'is_admin' => 1
        ]);
        factory('LojaVirtual\User', 5)->create();

    }

}