@extends('store.master')


@section('content')

    <section id="cart_items">
        <div class="container">

            <h2 class="title text-center">Entre com seu login e senha</h2>

            <div class="col-lg-6">

                @if($errors->first("fail"))
                    {{ $errors->first("fail") }}
                @endif

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div><i class="glyphicon glyphicon-alert"></i> {{ $error }}</div>
                        @endforeach
                    </div>
                @endif


                {!! Form::open(['route' => 'login.client.auth', 'role' => 'form', 'method' => 'post']) !!}
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" autofocus required/>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required/>
                </div>
                <div class="form-group col-lg-4">
                    <button type="submit" class="btn btn-success btn-block">Entrar</button>
                </div>
                {!! Form::close() !!}

            </div>

            <div class="col-lg-6">
                <a href="{{ route('client.create') }}"> Não é registrado? clique aqui.</a>
            </div>


        </div>
    </section>


@stop