@extends('store.master')

@section('slider_home')
    @include('store.partials.slider_home')
@stop


@section('category')
    @include('store.partials.category')
@stop

@section('content')


    <div class="col-sm-9 padding-right">
        <div class="features_items"><!--features_items-->
            <h2 class="title text-center">Em destaque</h2>

            @include('store.partials.products', ['products' => $pFeatured])

        </div>
        <!--features_items-->


        <div class="features_items"><!--recommended-->
            <h2 class="title text-center">Recomendados</h2>

            @include('store.partials.products', ['products' => $pRecommended])

        </div>
        <!--recommended-->

    </div>


@stop