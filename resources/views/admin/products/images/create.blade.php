@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Imagem
            <small>- Adicionar imagem para <strong>{{ $product->name }}</strong></small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Fazer Upload de Imagem</h3>
                    </div>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div><i class="glyphicon glyphicon-alert"></i> {{ $error }}</div>
                            @endforeach
                        </div>
                        @endif

                                <!-- /.box-header -->

                        <!-- form start -->
                        {!! Form::open(['files' => true, 'method' => 'post', 'route' => ['products.images.store',
                        $product->id]]) !!}
                        <div class="box-body">

                            <div class="form-group">
                                {!! Form::label('file', 'Imagem:') !!}
                                {!! Form::file('file', null, ['class' => 'form-control input-lg']) !!}
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                        {!! Form::close() !!}
                </div>
                <!-- /.box -->

            </div>
        </div>


    </div>


@stop