@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Imagens
            <small>- Imagens cadastradas para <strong>{{ $product->name }}</strong></small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">


                <div class="box">
                    <div class="box-header">
                        <div class="margin">
                            <a href="{{ route('products.images.create', $product->id) }}" class="btn btn-success"><i
                                        class="glyphicon glyphicon-plus"></i> Adicionar
                                Imagem</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Imagem</th>
                                <th>file</th>
                                <th>Ação</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($product->images as $image)
                                <tr>
                                    <td>{{ $image->id }}</td>
                                    <td><img src="{{ asset('uploads/'.$image->file) }}" class="thumbnail" width="200"/>
                                    </td>
                                    <td>{{ $image->file }}</td>
                                    <td>
                                        <a href="{{ route('products.images.destroy', ['id' => $image->id]) }}"
                                           class="btn btn-sm btn-danger">
                                            <i class="glyphicon glyphicon-remove"></i> Deletar
                                        </a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <a href="{{ route('products') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i>
                    Voltar</a>


            </div>
        </div>


    </div>


@stop