@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Produtos
            <small>- Criar novo produto</small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Novo Produto</h3>
                    </div>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div><i class="glyphicon glyphicon-alert"></i> {{ $error }}</div>
                            @endforeach
                        </div>
                        @endif

                                <!-- /.box-header -->

                        <!-- form start -->
                        {!! Form::open(['route' => 'products.store']) !!}
                        <div class="box-body">

                            <div class="form-group">
                                {!! Form::label('category_id', 'Categorias:') !!}
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control input-lg'])
                                !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('name', 'Nome:') !!}
                                {!! Form::text('name', null, ['class' => 'form-control input-lg']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('price', 'Preço:') !!}
                                {!! Form::text('price', null, ['class' => 'form-control input-lg']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('featured', 'Destaque:') !!}
                                {!! Form::select('featured', ['0' => 'Não', '1' => 'Sim'], '0') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('recommended', 'Recomendado:') !!}
                                {!! Form::select('recommended', ['0' => 'Não', '1' => 'Sim'], '0') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('description', 'Descrição:') !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('tags', 'Tags: (Digite a palavra e pressione ENTER)') !!}
                                {!! Form::text('tags', null, ['class' => 'form-control input-lg', 'data-role' => 'tagsinput']) !!}
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                        {!! Form::close() !!}
                </div>
                <!-- /.box -->

            </div>
        </div>


    </div>


@stop