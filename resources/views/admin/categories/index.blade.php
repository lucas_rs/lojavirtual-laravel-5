@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Categorias
            <small>- Categorias cadastradas no sistema</small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">


                <div class="box">
                    <div class="box-header">
                        <div class="margin">
                            <a href="{{ route('categories.create') }}" class="btn btn-success"><i
                                        class="glyphicon glyphicon-plus"></i> Adicionar
                                Categoria</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ação</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td>
                                        <a href="{{ route('categories.edit', ['id' => $category->id]) }}">
                                            <button class="btn btn-sm btn-warning">Editar</button>
                                        </a>
                                        <a href="{{ route('categories.destroy', ['id' => $category->id]) }}">
                                            <button class="btn btn-sm btn-danger">Deletar</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        {!! $categories->render() !!}

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


            </div>
        </div>


    </div>


@stop