@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Categories
            <small>- Editar categoria {{ $category->name }}</small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Categoria {{ $category->name }}</h3>
                    </div>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div><i class="glyphicon glyphicon-alert"></i> {{ $error }}</div>
                            @endforeach
                        </div>
                        @endif

                                <!-- /.box-header -->

                        <!-- form start -->
                        {!! Form::open(['route' => ['categories.update', $category->id], 'method'=> 'put']) !!}
                        <div class="box-body">

                            <div class="form-group">
                                {!! Form::label('name', 'Nome:') !!}
                                {!! Form::text('name', $category->name, ['class' => 'form-control input-lg']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description', 'Descrição:') !!}
                                {!! Form::textarea('description', $category->description, ['class' => 'form-control'])
                                !!}
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Atualizar</button>
                        </div>
                        {!! Form::close() !!}
                </div>
                <!-- /.box -->

            </div>
        </div>


    </div>


@stop